﻿using System.Linq;
using System.Threading.Tasks;
using Management.Database.Tables;
using Management.Infrastructure.Services.Employees;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net.Http.Headers;
using System.Data;
using System.Data.OleDb;
using LinqToExcel;
using System.Collections.Generic;

namespace EmployeeManagement.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class ImportExcelManager
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IEmployeeService Service;

        /// <summary>
        /// 
        /// </summary>
        private IFormFile _file;

        /// <summary>
        /// 
        /// </summary>
        private string _path;

        /// <summary>
        /// 
        /// </summary>
        private int _rowsCount;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public ImportExcelManager(IEmployeeService service)
        {
            Service = service;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="path"></param>
        /// <returns>Return how many rows are imported</returns>
        public async Task<List<Employee>> ImportAsync(IFormFile file, string path)
        {
            _file = file;
            _path = path;
            List<Employee> employees = new List<Employee>();

            CreateDirectoryIfNotExists();
            await CopyFileToLocalFolderAsync();

            var conString = GenerateConnectionString();
            DataTable dt = new DataTable();

            using (OleDbConnection connExcel = new OleDbConnection(conString))
            using (OleDbCommand cmdExcel = new OleDbCommand())
            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
            {
                cmdExcel.Connection = connExcel;
                var sheetName = GetFirstSheetName(connExcel);
                employees = GetEmployeesFromWorkSheet(sheetName);

                _rowsCount = 0;
                employees.ForEach(InsertEmployees);
                DeleteLocalFolder();
            }

            return employees;
        }

        #region InternalLogic

        /// <summary>
        /// 
        /// </summary>
        private void CreateDirectoryIfNotExists()
        {
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task CopyFileToLocalFolderAsync()
        {
            var filePath = GetFilePath();
            using (Stream stream = new FileStream(filePath, FileMode.Create))
                await _file.CopyToAsync(stream);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GenerateConnectionString()
        {
            var fileName = ContentDispositionHeaderValue.Parse(_file.ContentDisposition)
                .FileName.Trim('"');

            string extension = Path.GetExtension(fileName);
            string conString = string.Empty;

            switch (extension)
            {
                case ".xls": //Excel 97-03.
                    conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + GetFilePath() + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
                case ".xlsx": //Excel 07 and above.
                    conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + GetFilePath() + ";Extended Properties='Excel 8.0;HDR=YES'";
                    break;
            }

            conString = string.Format(conString, _path);
            return conString;
        }

        /// <summary>
        /// Get the name of First Sheet.
        /// </summary>
        /// <param name="connExcel"></param>
        /// <returns></returns>
        private string GetFirstSheetName(OleDbConnection connExcel)
        {
            DataTable dtExcelSchema;

            connExcel.Open();
            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString().Trim('$');
            connExcel.Close();

            return sheetName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        private List<Employee> GetEmployeesFromWorkSheet(string sheetName)
        {
            var excelFile = new ExcelQueryFactory(GetFilePath());
            var employees = (from emp in excelFile.Worksheet<Employee>(sheetName)
                             select emp).ToList();

            return employees;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private void InsertEmployees(Employee emp)
        {
            var employee = new Employee();
            employee.Number = emp.Number;
            employee.ForeNames = emp.ForeNames;
            employee.Surname = emp.Surname;
            employee.DateOfBirth = emp.DateOfBirth;
            employee.Telephone = emp.Telephone;
            employee.Mobile = emp.Mobile;
            employee.Address = emp.Address;
            employee.Address2 = emp.Address2;
            employee.PostCode = emp.PostCode;
            employee.EmailHome = emp.EmailHome;
            employee.StartDate = emp.StartDate;

            Service.AddOrUpdateAsync(employee).Wait();
            _rowsCount++;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DeleteLocalFolder()
        {
            if (File.Exists(GetFilePath()))
                File.Delete(GetFilePath());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetFilePath()
        {
            return Path.Combine(_path, _file.FileName);
        }

        #endregion
    }
}
