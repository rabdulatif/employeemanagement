﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Management.Database.Tables;
using Management.Infrastructure.Services.Employees;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using EmployeeManagement.Managers;

namespace EmployeeManagement.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class EmployeesController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IEmployeeService Service;

        /// <summary>
        /// 
        /// </summary>
        private readonly IHostingEnvironment _hostingEnvironment;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="hostingEnvironment"></param>
        public EmployeesController(IEmployeeService service, IHostingEnvironment hostingEnvironment)
        {
            Service = service;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadEmployees()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                // Skiping number of Rows count  
                var start = Request.Form["start"].FirstOrDefault();
                // Paging Length 10,20  
                var length = Request.Form["length"].FirstOrDefault();
                // Sort Column Name  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                // Sort Column Direction ( asc ,desc)  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                // Search Value from (Search box)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault();

                var order = Request.Form["order[0][column]"].FirstOrDefault();

                //Paging Size (10,20,50,100)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // Getting all Customer data  
                var employeeData = Service.GetQueryableEmployees();

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    InternalSortData(order, sortColumnDirection, ref employeeData);
                }

                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    employeeData = employeeData.Where(m => m.ForeNames.Contains(searchValue)
                                                    || m.Surname.Contains(searchValue)
                                                    || m.EmailHome.Contains(searchValue)
                                                    || m.Telephone.Contains(searchValue)
                                                    || m.Mobile.Contains(searchValue)
                                                    || m.PostCode.Contains(searchValue)
                                                    || m.Address.Contains(searchValue)
                                                    || m.Address2.Contains(searchValue)
                                                    || m.Number.Contains(searchValue));
                }

                //total number of rows count   
                recordsTotal = employeeData.Count();
                //Paging   
                var data = employeeData.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        /// <param name="employees"></param>
        private void InternalSortData(string order, string direction, ref IQueryable<Employee> employeeData)
        {
            switch (order)
            {
                case "2":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Number)
                        : employeeData.OrderBy(p => p.Number);
                    break;
                case "3":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.ForeNames)
                        : employeeData.OrderBy(p => p.ForeNames);
                    break;
                case "4":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Surname)
                        : employeeData.OrderBy(p => p.Surname);
                    break;
                case "5":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.DateOfBirth)
                        : employeeData.OrderBy(p => p.DateOfBirth);
                    break;
                case "6":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Telephone)
                        : employeeData.OrderBy(p => p.Telephone);
                    break;
                case "7":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Mobile)
                        : employeeData.OrderBy(p => p.Mobile);
                    break;
                case "8":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Address)
                        : employeeData.OrderBy(p => p.Address);
                    break;
                case "9":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.Address2)
                        : employeeData.OrderBy(p => p.Address2);
                    break;
                case "10":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.PostCode)
                        : employeeData.OrderBy(p => p.PostCode);
                    break;
                case "11":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.EmailHome)
                        : employeeData.OrderBy(p => p.EmailHome);
                    break;
                case "12":
                    employeeData = direction.Equals("DESC",
                        StringComparison.CurrentCultureIgnoreCase)
                        ? employeeData.OrderByDescending(p => p.StartDate)
                        : employeeData.OrderBy(p => p.StartDate);
                    break;
                default:
                    employeeData = employeeData.OrderBy(p => p.Surname);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await Service.GetOrderedEmployeesAsync());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
                return NotFound();

            var employee = await Service.GetEmployeeAsync(id);
            if (employee == null)
                return NotFound();

            return View(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeGuid,Number,ForeNames,Surname,DateOfBirth,Telephone,Mobile,Address,Address2,PostCode,EmailHome,StartDate,CreatedOn,ModifiedOn")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                await Service.AddOrUpdateAsync(employee);
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
                return NotFound();

            var employee = await Service.GetEmployeeAsync(id);
            if (employee == null)
                return NotFound();

            return View(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("EmployeeGuid,Number,ForeNames,Surname,DateOfBirth,Telephone,Mobile,Address,Address2,PostCode,EmailHome,StartDate,CreatedOn,ModifiedOn")] Employee employee)
        {
            if (id != employee.EmployeeGuid)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    await Service.AddOrUpdateAsync(employee);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Service.EmployeeExists(employee.EmployeeGuid))
                        return NotFound();
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task Import()
        {
            IFormFile file = Request.Form.Files[0];

            string folderName = "UploadExcel";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);

            var importManager = new ImportExcelManager(Service);
            var result = await importManager.ImportAsync(file, newPath);
            TempData["Message"] = $"{result.Count()} rows have been successfully added to the database";

        }
    }
}
