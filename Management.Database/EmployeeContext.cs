﻿using Management.Database.Tables;
using Microsoft.EntityFrameworkCore;

namespace Management.Database
{
    /// <summary>
    /// Database context
    /// </summary>
    public class EmployeeContext : DbContext
    {
        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="options"></param>
        public EmployeeContext(DbContextOptions<EmployeeContext> options) : base(options)
        {
            Database.Migrate();
        }

        /// <summary>
        /// Employees table
        /// </summary>
        public DbSet<Employee> Employees { get; set; }
    }
}
