﻿using System;

namespace Management.Database.Tables
{
    /// <summary>
    /// Audit base model class
    /// </summary>
    public class AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ModifiedOn { get; set; }
    }
}
