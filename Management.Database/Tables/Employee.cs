﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Database.Tables
{
    /// <summary>
    /// Employee table
    /// </summary>
    [Table("Employees")]
    public class Employee : AuditEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid EmployeeGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ForeNames { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayFormat(DataFormatString ="dd/mm/yyyy")]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Telephone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PostCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [EmailAddress]
        public string EmailHome { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate { get; set; }
    }
}
