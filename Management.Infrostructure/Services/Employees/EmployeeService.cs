﻿using Management.Database;
using Management.Database.Tables;
using Management.Infrastructure.Services.Abstracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Infrastructure.Services.Employees
{
    /// <summary>
    /// 
    /// </summary>
    public class EmployeeService : BaseService, IEmployeeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public EmployeeService(EmployeeContext context) : base(context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Employee>> GetOrderedEmployeesAsync()
        {
            return await Context.Employees.OrderBy(s => s.Surname).ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IQueryable<Employee> GetQueryableEmployees()
        {
            var res = from e in Context.Employees select e;
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeGuid"></param>
        /// <returns></returns>
        public async Task<Employee> GetEmployeeAsync(Guid? employeeGuid)
        {
            if (employeeGuid == null)
                return null;

            return await Context.Employees.FirstOrDefaultAsync(f => f.EmployeeGuid == employeeGuid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        public async Task AddOrUpdateAsync(Employee employee)
        {
            if (employee.EmployeeGuid == Guid.Empty)
                InternalAdd(employee);
            else
                InternalUpdate(employee);

            await Context.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        private void InternalAdd(Employee employee)
        {
            employee.EmployeeGuid = Guid.NewGuid();
            employee.CreatedOn = DateTime.Now;

            Context.Add(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        private void InternalUpdate(Employee employee)
        {
            employee.ModifiedOn = DateTime.Now;
            Context.Update(employee);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        public async Task RemoveAsync(Employee employee)
        {
            Context.Remove(employee);
            await Context.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeGuid"></param>
        public async Task RemoveAsync(Guid employeeGuid)
        {
            var employee = FindAsync(employeeGuid);
            Context.Remove(employee);

            await Context.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Employee> FindAsync(Guid id)
        {
            return await Context.Employees.FindAsync(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool EmployeeExists(Guid id)
        {
            return Context.Employees.Any(e => e.EmployeeGuid == id);
        }
    }
}
