﻿using Management.Database.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Infrastructure.Services.Employees
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEmployeeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Employee>> GetOrderedEmployeesAsync();
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<Employee> GetQueryableEmployees();
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeGuid"></param>
        /// <returns></returns>
        Task<Employee> GetEmployeeAsync(Guid? employeeGuid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        Task AddOrUpdateAsync(Employee employee);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employee"></param>
        Task RemoveAsync(Employee employee);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeGuid"></param>
        Task RemoveAsync(Guid employeeGuid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Employee> FindAsync(Guid id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool EmployeeExists(Guid id);
    }
}
