﻿using Management.Database;

namespace Management.Infrastructure.Services.Abstracts
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseService
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly EmployeeContext Context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public BaseService(EmployeeContext context)
        {
            Context = context;
        }
    }
}
